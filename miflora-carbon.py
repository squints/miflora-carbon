#!/usr/bin/python3

from miflora.miflora_poller import MiFloraPoller, MI_CONDUCTIVITY, MI_MOISTURE, MI_LIGHT, MI_TEMPERATURE, MI_BATTERY
from btlewrap import BluepyBackend
import time
import graphyte
import json
import os

def main():
    __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    with open(os.path.join(__location__, 'conf.json')) as f:
        data = json.load(f)
    graphyte.init(data["server"], prefix='Tree')
    while True:
        poller = MiFloraPoller(data["MAC"], BluepyBackend)
        print("Getting data from Mi Flora")
        print("FW: {}".format(poller.firmware_version()))
        print("Name: {}".format(poller.name()))
        print("Temperature: {}".format((1.8*poller.parameter_value(MI_TEMPERATURE))+32))
        print("Moisture: {}".format(poller.parameter_value(MI_MOISTURE)))
        print("Light: {}".format(poller.parameter_value(MI_LIGHT)))
        print("Conductivity: {}".format(poller.parameter_value(MI_CONDUCTIVITY)))
        print("Battery: {}".format(poller.parameter_value(MI_BATTERY)))
        print("Sending to carbon")
        graphyte.send("Temperature",  (1.8*poller.parameter_value(MI_TEMPERATURE))+32 )
        graphyte.send("Moisture", poller.parameter_value(MI_MOISTURE) )
        graphyte.send("Light", poller.parameter_value(MI_LIGHT) )
        graphyte.send("Conductivity", poller.parameter_value(MI_CONDUCTIVITY) )
        graphyte.send("Battery", poller.parameter_value(MI_BATTERY) )
        print("Sleeping " + str(data["delay"]) + "s...")
        time.sleep(data["delay"])

if __name__ == '__main__':
    main()
